package com.test.pluto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by sujit on 5/9/19.
 */
@RequestMapping("")
@Controller
public class HelloController {
    @Autowired
    private  HelloService helloService;

    @RequestMapping(method = RequestMethod.GET)
    public String printHello(ModelMap model) {
        model.addAttribute("message", "Hello Spring MVC Framework");
        model.addAttribute("employees",helloService.getAllEmployee());
        return "hello";
    }
}

