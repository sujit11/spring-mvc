package com.test.pluto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujit on 5/10/19.
 */
@Service
public class HelloService {
    @Autowired
    private HelloDao helloDao;

    public List<String> getAllEmployee() {
        return helloDao.getAllEmployee();
    }
}
