package com.test.pluto;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sujit on 5/10/19.
 */
@Repository
public class HelloDao {

    public List<String> getAllEmployee(){
        List<String> employees = new ArrayList();
        employees.add("Tashi");
        employees.add("Dawa");
        employees.add("Tshering");
        employees.add("Sonam");
        employees.add("Dema");
        return employees;
    }
}
